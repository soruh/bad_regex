use std::{
    collections::HashMap,
    fmt::{Display, Write},
    num::NonZeroUsize,
    ops::{Add, AddAssign, BitOr, BitOrAssign, Mul},
    rc::{Rc, Weak},
};

use crate::syntax::{Ast, Class, Part, PartBody, Segment};

// TODO: these could be `smallvec`s
#[derive(Debug, Clone, Default)]
pub struct Nfa {
    states: Vec<State>,
    start: usize,
    accepting: Vec<usize>,
}

impl Display for Nfa {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "digraph {{")?;

        writeln!(f, "    START [label=\"\" shape=none]")?;
        writeln!(f, "    START -> {}[label=\"start\"];", self.start)?;

        for (i, state) in self.states.iter().enumerate() {
            writeln!(
                f,
                "    {name}[shape=\"{shape}\"]",
                name = i,
                shape = if self.accepting.contains(&i) {
                    "doublecircle"
                } else {
                    "circle"
                },
            )?;
            for transition in &state.transitions {
                writeln!(
                    f,
                    "    {from} -> {to}[label=\"{label}\"];",
                    from = i,
                    to = transition.target,
                    label = transition.input,
                )?;
            }
        }

        writeln!(f, "}}")?;

        Ok(())
    }
}

#[derive(Debug, Clone, Default)]
struct State {
    transitions: Vec<Transition>,
}

#[derive(PartialOrd, Ord, PartialEq, Eq, Clone, Debug)]
struct Transition {
    input: Class,
    target: usize,
}

impl Nfa {
    pub fn accepts_epsilon(&self) -> bool {
        self.accepting.contains(&self.start)
    }

    pub fn accept_epsilon() -> Self {
        Nfa {
            states: vec![State {
                transitions: vec![],
            }],
            start: 0,
            accepting: vec![0],
        }
    }

    pub fn from_ast(ast: &Ast) -> Option<Self> {
        let mut nfa: Option<Self> = None;

        for segment in &ast.segments {
            let segment_nfa = Nfa::from_segment(segment);

            nfa = Some(if let Some(nfa) = nfa {
                nfa | segment_nfa
            } else {
                segment_nfa
            });
        }

        nfa
    }

    pub fn from_segment(segment: &Segment) -> Self {
        let mut output_nfa = None;

        for part in &segment.parts {
            let part_nfa = Nfa::from_part(part);

            output_nfa = Some(if let Some(other) = output_nfa {
                other + part_nfa
            } else {
                part_nfa
            });
        }

        output_nfa.unwrap_or_else(|| Nfa {
            states: vec![State::default()],
            start: 0,
            accepting: vec![0],
        })
    }

    pub fn from_part(part: &Part) -> Self {
        let mut body = Nfa::from_part_body(&part.body);

        match part.quantifier {
            (n, None) => {
                let mut body = if n == 0 {
                    // TODO: this check is probably unncecessary
                    if !body.accepting.contains(&body.start) {
                        // also accept no transition
                        body.accepting.push(body.start);
                    }

                    body
                } else {
                    body * n
                };

                // adjust all transitions to an accepting state also point to the start state
                for &accepting in &body.accepting {
                    for state in &mut body.states {
                        let mut new_state_transitions = Vec::new();

                        for transition in &mut state.transitions {
                            if transition.target == accepting {
                                let mut transition = transition.clone();
                                transition.target = body.start;
                                new_state_transitions.push(transition);
                            }
                        }

                        // append and deduplicate transitions
                        state.transitions.append(&mut new_state_transitions);
                        state.transitions.sort_unstable();
                        state.transitions.dedup();
                    }
                }

                body
            }

            (1, Some(n)) if n.get() == 1 => body,

            (mut a, Some(b)) => {
                let b = b.get();
                assert!(b >= a);

                let mut res = body.clone() * a;

                if b > a {
                    let mut tail = None;

                    for i in 0..=b - a {
                        let body = if i == b - a {
                            std::mem::take(&mut body)
                        } else {
                            body.clone()
                        };

                        let body = body * i;

                        if let Some(tail) = tail.as_mut() {
                            *tail |= body;
                        } else {
                            tail = Some(body);
                        }
                    }

                    res += tail.unwrap();
                }

                res
            }
        }
    }

    pub fn from_part_body(part: &PartBody) -> Self {
        match part {
            PartBody::SubMatch(ast) => Nfa::from_ast(ast).expect("empty parenthesis?"),
            PartBody::Class(class) => Nfa {
                states: vec![
                    State {
                        transitions: vec![Transition {
                            input: class.clone(),
                            target: 1,
                        }],
                    },
                    State {
                        transitions: vec![],
                    },
                ],
                start: 0,
                accepting: vec![1],
            },
        }
    }

    fn swap_start_to_0(&mut self) {
        if self.start != 0 {
            // adjust all transitions to the start state or the state at 0 to point to their new locations.
            for state in &mut self.states {
                for transition in &mut state.transitions {
                    if transition.target == 0 {
                        transition.target = self.start;
                    } else if transition.target == self.start {
                        transition.target = 0;
                    }
                }
            }

            for accepting in &mut self.accepting {
                if *accepting == 0 {
                    *accepting = self.start;
                } else if *accepting == self.start {
                    *accepting = 0;
                }
            }

            self.states.swap(self.start, 0);
            self.start = 0;
        }
    }

    pub fn inverted(&self) -> Nfa {
        let mut inverted = Nfa {
            states: vec![
                State {
                    transitions: vec![]
                };
                self.states.len()
            ],
            start: self.start,
            accepting: self.accepting.clone(),
        };

        for (i, state) in self.states.iter().enumerate() {
            for mut transition in state.transitions.iter().cloned() {
                let old_target = transition.target;
                transition.target = i;
                inverted.states[old_target].transitions.push(transition);
            }
        }

        inverted
    }

    pub fn prune_unreachable(&mut self) {
        let mut unreachable = Vec::<usize>::new();

        let inverted = self.inverted();

        for (i, state) in inverted.states.iter().enumerate() {
            if state.transitions.is_empty() && i != inverted.start {
                unreachable.push(i);
            }
        }

        if unreachable.is_empty() {
            return;
        }

        unreachable.sort_unstable();

        // calculate positions of nodes after removing unreachable elements
        let mut new_state_positions: Vec<_> = self
            .states
            .iter()
            .enumerate()
            .map(|(state_index, state)| {
                let n_removed_before = unreachable.iter().take_while(|&&i| i < state_index).count();

                state_index - n_removed_before
            })
            .collect();

        // adjust transition targets to their post-removal positions
        for state in &mut self.states {
            for transition in &mut state.transitions {
                transition.target = new_state_positions[transition.target];
            }
        }

        // adjust accepting states to their post-removal positions
        for accepting in &mut self.accepting {
            *accepting = new_state_positions[*accepting];
        }

        // adjust the start state to its post-removal position
        self.start = new_state_positions[self.start];

        // remove the unreachable states from the back to make sure the other unreachable indecies are still valid
        for &unreachable in unreachable.iter().rev() {
            self.states.remove(unreachable);
        }
    }

    pub fn to_dfa(&self) -> Self {
        // TOOD: use a faster hashing alogorithm
        let mut reachable_states = HashMap::<Vec<usize>, usize>::new();

        let mut dfa = Nfa {
            states: vec![State {
                transitions: vec![],
            }],
            start: 0,
            accepting: vec![],
        };

        todo!()
    }
}

impl Add for Nfa {
    type Output = Nfa;

    fn add(self, mut rhs: Self) -> Self::Output {
        let mut lhs = self;

        let rhs_accepted_epsilon = rhs.accepts_epsilon();

        // adjust all rhs transitions to point to same node when appended to lhs
        for state in &mut rhs.states {
            for transition in &mut state.transitions {
                transition.target += lhs.states.len();
            }
        }

        // add all transitions from the rhs start state to each accepting state of the lhs
        for &accepting in &lhs.accepting {
            for mut transition in rhs.states[rhs.start].transitions.iter().cloned() {
                // adjust transition to point to same node when appended to lhs
                lhs.states[accepting].transitions.push(transition);
            }
        }

        // the new accepting states are the accepting states of the rhs

        for accepting in &mut rhs.accepting {
            *accepting += lhs.states.len();
        }

        if rhs_accepted_epsilon {
            // if the rhs accepted no input the lhs accepting states need to stay accepting
            lhs.accepting.append(&mut rhs.accepting);
        } else {
            // if the rhs did not accept no input the lhs accepting states must no longer be accepting
            // NOTE: we do this instead of moving each element sepearately to keep the same allocation
            // as its size is already correct
            lhs.accepting = std::mem::take(&mut rhs.accepting);
        }

        lhs.states.append(&mut rhs.states);

        // lhs.start stays the same

        lhs
    }
}

impl AddAssign for Nfa {
    fn add_assign(&mut self, rhs: Self) {
        let x = std::mem::take(self);
        *self = x + rhs;
    }
}

impl Mul<usize> for Nfa {
    type Output = Nfa;

    fn mul(self, n: usize) -> Self::Output {
        if n == 0 {
            Nfa::accept_epsilon()
        } else if n == 1 {
            self
        } else {
            let mut res = self.clone();
            for i in 1..n {
                if i == n - 1 {
                    res += self;
                    break; // this break is here to tell the borrow checker that the loop is over
                } else {
                    res += self.clone();
                }
            }

            res
        }
    }
}

impl BitOrAssign for Nfa {
    fn bitor_assign(&mut self, rhs: Self) {
        let x = std::mem::take(self);
        *self = x | rhs;
    }
}

impl BitOr for Nfa {
    type Output = Self;

    // NOTE: after calling this the start state will be the last state
    #[allow(clippy::suspicious_arithmetic_impl)]
    fn bitor(self, mut rhs: Self) -> Self::Output {
        let mut lhs = self;

        let should_accept_epsilon = lhs.accepts_epsilon() || rhs.accepts_epsilon();

        for state in &mut rhs.states {
            for transition in &mut state.transitions {
                transition.target += lhs.states.len();
            }
        }

        lhs.accepting.reserve(rhs.accepting.len());
        for accepting in rhs.accepting {
            lhs.accepting.push(accepting + lhs.states.len());
        }

        let new_start = State {
            transitions: lhs.states[lhs.start]
                .transitions
                .iter()
                .chain(rhs.states[rhs.start].transitions.iter())
                .cloned()
                .collect(),
        };

        lhs.states.reserve(rhs.states.len() + 1);
        lhs.states.append(&mut rhs.states);

        lhs.start = lhs.states.len();
        lhs.states.push(new_start);

        if should_accept_epsilon && !lhs.accepts_epsilon() {
            lhs.accepting.push(lhs.start);
        }

        lhs
    }
}

#[cfg(test)]
mod test {
    use super::*;

    macro_rules! state {
        ($($char: literal => $target: literal),* $(,)?) => {
            State {
                transitions: vec![$(
                    Transition {
                        input: Class {

                            parts: vec![($char, $char)],
                        },
                        target: $target,
                    }
                ),*],
            }
        };
    }

    #[test]
    #[ignore]
    fn adding_nfas() {
        let a = Nfa {
            states: vec![
                state! {'0' => 1, '1' => 2},
                state! {'0' => 0, '1' => 3},
                state! {'0' => 3, '1' => 0},
                state! {'0' => 2, '1' => 1},
            ],
            start: 0,
            accepting: vec![0],
        };

        println!("{}", a);

        let b = Nfa {
            states: vec![
                state! {'0' => 1},
                state! {'0' => 2},
                state! {'0' => 3},
                state! {},
            ],
            start: 0,
            accepting: vec![3],
        };

        println!("{}", b);

        let c = b + a;

        println!("{}", c);

        // TODO: check the result is correct (instead of just printing it)
    }

    #[test]
    #[ignore]
    fn multiplying_nfas() {
        let a = Nfa {
            states: vec![
                state! {'0' => 1},
                state! {'0' => 2},
                state! {'0' => 3},
                state! {},
            ],
            start: 0,
            accepting: vec![3],
        };

        let mut c = a.clone();

        for n in 2..4 {
            c |= a.clone() * n;
        }

        let b = Nfa {
            states: vec![
                state! {'1' => 1},
                state! {'1' => 2},
                state! {'1' => 3},
                state! {},
            ],
            start: 0,
            accepting: vec![3],
        };

        let mut d = c + b;

        eprintln!("d");
        println!("{}", d);

        eprintln!("d inverted");
        println!("{}", d.inverted());

        eprintln!("d pruned");
        d.prune_unreachable();
        println!("{}", d);

        todo!()
    }
}
