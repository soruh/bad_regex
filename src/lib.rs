#![feature(destructuring_assignment)]
#![allow(unused)]

pub mod dfa;
pub mod matching;
pub mod nfa;
pub mod syntax;

#[cfg(test)]
mod tests {
    #[test]
    fn parse_all_features() {
        // const INPUT: &str = "a*b?c{2,}d{4,9}(123)+(45+6)?:[1-9][0-9]* met?al towe(r|l) a(bc|bd)";
        const INPUT: &str = "x?y*z";
        // const INPUT: &str = "[a0-91-2]";

        let ast = crate::syntax::Ast::parse(INPUT).expect("Failed to parse input");

        dbg!(&ast);

        println!("AST: {}", ast.print());

        let mut nfa = crate::nfa::Nfa::from_ast(&ast).expect("Failed to build NFA");

        nfa.prune_unreachable();

        let dfa = nfa.to_dfa();

        {
            use std::io::Write;
            let mut file = std::fs::File::create("./graph.dot").unwrap();
            write!(file, "{}", dfa);
        }

        panic!();
    }
}
