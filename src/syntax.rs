use std::{fmt::Display, num::NonZeroUsize};

#[derive(Debug)]
pub struct Ast {
    pub segments: Vec<Segment>,
}

impl Ast {
    pub fn print(&self) -> String {
        self.segments
            .iter()
            .map(|x| format!("({})", x.print()))
            .collect::<Vec<String>>()
            .join("|")
    }
}

#[derive(Debug)]
pub struct Segment {
    pub parts: Vec<Part>,
}

impl Segment {
    fn print(&self) -> String {
        self.parts
            .iter()
            .map(|x| format!("({})", x.print()))
            .collect::<String>()
    }
}

#[derive(Debug)]
pub struct Part {
    pub body: PartBody,
    pub quantifier: (usize, Option<NonZeroUsize>),
}

impl Part {
    fn print(&self) -> String {
        let body = match &self.body {
            PartBody::SubMatch(ast) => ast.print(),
            PartBody::Class(class) => format!("{}", class),
        };

        let quantifier = match (self.quantifier.0, self.quantifier.1.map(|x| x.get())) {
            (1, Some(1)) => return body,
            (0, Some(1)) => "?".to_owned(),
            (0, None) => "*".to_owned(),
            (1, None) => "+".to_owned(),
            (a, Some(b)) => format!("{{{}, {}}}", a, b),
            (a, None) => format!("{{{}, }}", a),
        };

        format!("({}){}", body, quantifier)
    }
}

fn concat_char_ranges(a: (char, char), b: (char, char)) -> Option<(char, char)> {
    let ranges_overlap = a.0.max(b.0) <= a.1.min(b.1);

    if ranges_overlap {
        Some((a.0.min(b.0), a.1.max(b.1)))
    } else {
        None
    }
}

impl Class {
    fn simplify(&mut self) {
        self.parts.sort_unstable();

        self.parts = std::mem::take(&mut self.parts)
            .into_iter()
            .fold(vec![], |mut acc, val| {
                if let Some(last) = acc.last_mut() {
                    if let Some(combined) = concat_char_ranges(*last, val) {
                        *last = combined;
                        return acc;
                    }
                }

                acc.push(val);
                acc
            });
    }
    fn union(mut self, mut other: Self) -> Self {
        self.parts.append(&mut other.parts);
        self.simplify();
        self
    }
    fn intersection(self, other: Self) -> Self {
        todo!()
    }
    fn difference(self, other: Self) -> Self {
        todo!()
    }
}

#[derive(Debug)]
pub enum PartBody {
    SubMatch(Ast),
    Class(Class),
}

#[derive(PartialOrd, Ord, PartialEq, Eq, Clone, Debug)]
pub struct Class {
    pub parts: Vec<(char, char)>,
}

impl Display for Class {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let body = self
            .parts
            .iter()
            .map(|&(a, b)| {
                if a == b {
                    a.to_string()
                } else {
                    format!("{}-{}", a, b)
                }
            })
            .collect::<String>();

        write!(f, "{}", body)
    }
}

fn parse_body(input: &str) -> Option<(PartBody, &str)> {
    Some(match input.chars().next()? {
        '*' | '+' | '{' | '}' | '|' => return None,

        '(' => {
            let i = input.find(')')?;
            let body = &input[1..i];

            (PartBody::SubMatch(Ast::parse(body)?), &input[i + 1..])
        }
        '[' => {
            let i = input.find(']')?;
            let mut body = &input[1..i];

            let mut parts = Vec::new();

            let mut chars = body.chars().peekable();
            while let Some(start) = chars.next() {
                if start == '-' {
                    return None;
                }

                if chars.peek() == Some(&'-') {
                    let _ = chars.next();
                    let end = chars.next()?;

                    parts.push((start, end));
                } else {
                    parts.push((start, start));
                }
            }

            let mut class = Class { parts };
            class.simplify();
            (PartBody::Class(class), &input[i + 1..])
        }

        c => (
            PartBody::Class(Class {
                parts: vec![(c, c)],
            }),
            &input[1..],
        ),
    })
}

fn parse_quantifier(input: &str) -> ((usize, Option<NonZeroUsize>), &str) {
    input
        .chars()
        .next()
        .and_then(|first_char| match first_char {
            '*' => Some(((0, None), &input[1..])),
            '+' => Some(((1, None), &input[1..])),
            '?' => Some(((0, NonZeroUsize::new(1)), &input[1..])),
            '{' => {
                let i = input.find('}')?;
                let body = &input[1..i];

                let mut parts = body.split(',');
                let start: usize = parts.next()?.trim().parse().ok()?;
                let end: Option<NonZeroUsize> = parts.next().and_then(|x| x.trim().parse().ok());

                if parts.next().is_some() {
                    return None;
                }

                Some(((start, end), &input[i + 1..]))
            }

            _ => None,
        })
        .unwrap_or(((1, NonZeroUsize::new(1)), input))
}

fn parse_part(mut input: &str) -> Option<(Part, &str)> {
    let body;
    (body, input) = parse_body(input)?;

    let quantifier;
    (quantifier, input) = parse_quantifier(input);

    Some((Part { body, quantifier }, input))
}

impl Ast {
    pub fn parse(mut input: &str) -> Option<Self> {
        assert!(input.is_ascii());

        let mut parts = Vec::new();
        let mut segments = Vec::new();

        while !input.is_empty() {
            let part;
            (part, input) = parse_part(input)?;

            parts.push(part);

            if let Some(new_input) = input.strip_prefix('|') {
                input = new_input;

                segments.push(Segment { parts });
                parts = Vec::new();
            }
        }

        if parts.is_empty() {
            return None;
        }

        segments.push(Segment { parts });

        Some(Self { segments })
    }
}
